# ep3_icf
## Primeira utilização
* Depois de dado o comando:

        git clone https://gitlab.com/pmiranda7410/ep3_icf 

* Dentro da pasta, onde se encontra os arquivos do rails, dê o comando:

        rake db:migrate
*  E logo depois:

        bundle install
    
* Ele criará todas as colunas da tabela do banco de dados.

## Navegando no site
* Utilize o comando para inicializar o servidor:
        
        rails s

## Acesse a página localhost:3000 no seu computador, ou somente 0.0.0.0:3000 e você se depará com isso:

![N|Solid](https://gitlab.com/pmiranda7410/ep3_icf/raw/master/fotosREADME/img_1.png)

## Nessa mesma página, possui dois botõs para efetuar o login, tanto no meio chamado "Sign in" quanto no simbolo de uma cabeça no canto direito em cima:

![N|Solid](https://gitlab.com/pmiranda7410/ep3_icf/raw/master/fotosREADME/img_2.png)

## Clicando no "Sign in" você será redirecionado para uma página de login:

![N|Solid](https://gitlab.com/pmiranda7410/ep3_icf/raw/master/fotosREADME/img_3.png)

## Caso ja possua conta, apenas efetua o login, caso contrário clique em "sign up" em cima da barra de input do e-mail e você se depará com esta página de cadastro:

![N|Solid](https://gitlab.com/pmiranda7410/ep3_icf/raw/master/fotosREADME/img_4.png)

![N|Solid](https://gitlab.com/pmiranda7410/ep3_icf/raw/master/fotosREADME/img_5.png)

## Depois de feito o cadastro e apertado no botão "Sign up" você será redirecionado para a home page:

![N|Solid](https://gitlab.com/pmiranda7410/ep3_icf/raw/master/fotosREADME/img_6.png)

## Dentro da Home Page, você pode clicar nas opções de engenharia na lateral esquerda, escolher a quantidade de cursos que deseja ver a quantidade de alunos clicando no botão atualizar:

![N|Solid](https://gitlab.com/pmiranda7410/ep3_icf/raw/master/fotosREADME/img_7.png)

## -

![N|Solid](https://gitlab.com/pmiranda7410/ep3_icf/raw/master/fotosREADME/img_8.png)

## Caso queira desconectar-se, apenas clicar na opção com o icone de uma cabeça na lateral direita em cima e selecionar "Logout":

![N|Solid](https://gitlab.com/pmiranda7410/ep3_icf/raw/master/fotosREADME/img_9.png)
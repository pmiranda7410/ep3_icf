class AllSpecialties < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :graduation, :string
  	add_column :users, :python, :boolean
    add_column :users, :c, :boolean
    add_column :users, :php, :boolean
    add_column :users, :data_base, :boolean
    add_column :users, :java_script, :boolean
    add_column :users, :estrutura, :boolean
    add_column :users, :propulsao, :boolean
    add_column :users, :controle, :boolean
    add_column :users, :design, :boolean
    add_column :users, :micro_processadores, :boolean
    add_column :users, :micro_controladores, :boolean
    add_column :users, :embarcados, :boolean
    add_column :users, :eletronica_digital, :boolean
    add_column :users, :auto_design, :boolean
	add_column :users, :autonomia, :boolean
	add_column :users, :mecanica, :boolean
	add_column :users, :fontes_renovaveis, :boolean
	add_column :users, :petroleo, :boolean
    add_column :users, :hidreletric, :boolean
    add_column :users, :auto_structure, :boolean
    add_column :users, :suspension, :boolean
	end
end

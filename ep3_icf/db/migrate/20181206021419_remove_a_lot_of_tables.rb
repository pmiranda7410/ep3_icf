class RemoveALotOfTables < ActiveRecord::Migration[5.2]
  def change
    remove_column :users, :c, :boolean
    remove_column :users, :php, :boolean
    remove_column :users, :data_base, :boolean
    remove_column :users, :java_script, :boolean
    remove_column :users, :micro_processadores, :boolean
    remove_column :users, :micro_controladores, :boolean
    remove_column :users, :auto_design, :boolean
	remove_column :users, :autonomia, :boolean
	remove_column :users, :mecanica, :boolean
    remove_column :users, :hidreletric, :boolean
    remove_column :users, :auto_structure, :boolean
  end
end

class RegistrationsController < Devise::RegistrationsController
	def edit
  		super
  	end
  private

  def sign_up_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :java, :graduation, :python, :estrutura, :propulsao, :controle, :design, :embarcados, :eletronica_digital, :fontes_renovaveis, :petroleo, :suspension, :teacher)
  end

  def account_update_params
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :java, :graduation, :python, :estrutura, :propulsao, :controle, :design, :embarcados, :eletronica_digital, :fontes_renovaveis, :petroleo, :suspension, :current_password)
  end

end